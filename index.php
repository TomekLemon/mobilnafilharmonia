<?php setcookie("youtube", true); ?>
<!DOCTYPE html>
<html>
<head lang="pl">
    <title>Mobilna Filharmonia / koncerty tematyczne</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="Description" content="Mobilna Filharmonia to organizator nowatorskich lekcji muzyki w szkołach i przedszkolach, a także tematycznych koncertów i imprez okolicznosciowych." />
    <meta name="Keywords" content="audycje muzyczne, teatrzyk dla przedszkoli, wesela ludowe, oprawa ślubów i wesel" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta property="og:title" content="Mobilna Filharmonia / koncerty tematyczne" />
    <!-- NEXT LINE Even if page is dynamically generated and URL contains query parameters -->
    <meta property="og:url" content="http://www.mobilnafilharmonia.pl" />
    <meta property="og:image" content="http://www.mobilnafilharmonia.pl/images/fb-share.jpg" />

    <link href="js/qtip/jquery.qtip.min.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="css/style.css?ver=1.2.32" rel="stylesheet" type="text/css" media="all" />
    <link rel="icon" type="image/x-icon" href="favicon.png" />
    <link rel="image_src" type="image/jpeg" href="http://www.mobilnafilharmonia.pl/images/fb-share.jpg" />
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '698362696978304'); // Insert your pixel ID here.
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=698362696978304&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
</head>
<body>
<script>
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.4";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<div id="content-wrapper">
    <div id="fb">
        <div class="fb-tab"></div>
        <div class="fb-page" data-href="https://www.facebook.com/mobilnafilharmoniarzeszow" data-small-header="false" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="true" data-show-posts="false">
            <div class="fb-xfbml-parse-ignore">
                <blockquote cite="https://www.facebook.com/mobilnafilharmoniarzeszow">
                    <a href="https://www.facebook.com/mobilnafilharmoniarzeszow">Mobilna Filharmonia</a>
                </blockquote>
            </div>
        </div>
    </div>
    <div id="top">
        <div class="content">
            <div id="top-title">
                <img src="images/top_title.png" alt="Mobilna Filharmonia">
            </div>
            <a id="show-kontakt" href="#kontakt">Skontaktuj się<br />z nami</a>
        </div>
    </div>
    <div id="o-nas">
        <div class="content">
            <div id="o-nas-title">
                <img src="images/onas_title.png" alt="O nas">
            </div>
            <div class="text-top">
                <h3>
                    Mobilna Filharmonia organizator nowatorskich lekcji muzyki w szkołach i w przedszkolach
                </h3>
                <p>Dbając o wszechstronny rozwój dzieci i młodzieży, proponujemy cykl tematycznych koncertów, w których Mobilna Filharmonia prezentuje bogactwo polskiej i światowej kultury muzycznej.</p>

                <p>Razem z dziećmi odkrywamy tajniki muzyki, jej style, epoki historyczne. Poznajemy ciekawostki z życia słynnych kompozytorów, uczymy się gry na różnych instrumentach i wspólnie śpiewamy.</p>

                <p>Koncerty wykonywane są na żywo przez osoby z wykształceniem muzycznym i pedagogicznym. Staramy się, aby każdy koncert był inny, każdy 
z muzyków był odpowiednio ucharakteryzowany, a na kolejnych audycjach pojawiał się nowy instrument muzyczny oraz adekwatne do tematu  i epoki rekwizyty.</p>

                <p>Każdy temat dostosowany jest do wieku uczestników i oparty jest na nowej podstawie programowej Ministerstwa Edukacji Narodowej oraz nawiązuje do aktualnie obchodzonych świąt takich jak Święto Odzyskania Niepodległości czy Boże Narodzenie. </p>
                <p>Uśmiech i zadowolenie dzieci i młodzieży sprawiają, że przygoda w świecie melodii, brzmień i odgłosów poza walorami wychowawczymi ma także inny aspekt – dobrego samopoczucia. </p>

                <p>Zapraszamy do współpracy w nowym sezonie artystycznym 2017/2018.</p>

                <p>Cena audycji ustalana jest indywidualnie.</p>
                <p class="bigger">Napisz do nas!</p>
            </div>
            <div class="opcje">
                <p>Czas trwania koncertów:</p>
                <div class="items">
                    <div class="item szkoly">
                        <div class="icon">
                            <div class="top">45</div>
                            <div class="bottom">min</div>
                        </div>
                        <div class="text">w szkołach</div>
                    </div>
                    <div class="item przedszkola">
                        <div class="icon">
                            <div class="top">30</div>
                            <div class="bottom">min</div>
                        </div>
                        <div class="text">w przedszkolach</div>
                    </div>
                    <div class="item dorosli">
                        <div class="icon">
                            <div class="top">60</div>
                            <div class="bottom">min</div>
                        </div>
                        <div class="text">dla dorosłych</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="tematy">
        <div class="content">
            <div id="tematy-title">
                <img src="images/tematy_title.png" alt="Proponowane tematy">
            </div>
            <ul class="unstyled">
                <li>
                    <div class="month"></div>
                    <p class="title">1. W muzycznej podróży świat nie jest zbyt duży.</p>
                    <p>
                        W pamięci mamy jeszcze wspomnienie wakacji, zatem niech muzyka zabierze nas z powrotem do miejsc niezwykłych i obfitujących w pełną gamę egzotycznych dźwięków.
                    </p>
                    <p class="instruments">
                        instrumenty: gitara, ukulele, flet poprzeczny, i in.
                    </p>
                </li>
                <li>
                    <div class="month"></div>
                    <p class="title">2. Świąteczne zwyczaje te duże i małe. </p>
                    <p>
                        Mikołaj, Gwiazdor, Dziadek Mróz, a może Aniołek? Kto tak naprawdę przynosi prezenty? Co śpiewamy przy wigilijnym stole? Czy u wszystkich na świecie Boże Narodzenie jest zimą? Na te i inne pytania postaramy się śpiewająco odpowiedzieć.
                    </p>
                    <p class="instruments">
                        instrumenty: altówka, cyfrowe pianino, skrzypce i in.
                    </p>
                </li>
                <li>
                    <div class="month"></div>
                    <p class="title">3. Muzyczne chichoty, żarty i psoty. </p>
                    <p>
                       Muzyka poważna niepoważnie? Tak! Z żartem i dowcipem zaprezentujemy nietypowe wykorzystanie instrumentów oraz utwory pełne figlów. Będzie wesoło i zaskakująco!
                    </p>
                    <p class="instruments">
                        instrumenty: maszyna do pisania, tarka do gipsu i in. ;)
                    </p>
                </li>
                <li>
                    <div class="month"></div>
                    <p class="title">4. Folkloru bezdroża od gór aż do morza.</p>
                    <p>
                       W podróży po Polsce poznamy przyśpiewki, tańce, zabawy oraz barwne stroje różnych regionów. Czarować nas będą dźwięki niezwykłego instrumentu - to cymbały (nie mylić z dzwonkami). Grał na nich Jankiel, a może podczas audycji odkryjemy nowy talent??
                    </p>
                    <p class="instruments">
                        instrumenty: cymbały, skrzypce, flet i in.
                    </p>
                </li>
            </ul>

        </div>
    </div>
    <div id="after-tematy">
        <div class="content">
            <div class="gallery-photo-bg"></div>
            <div id="tematy-after-title">
                <img src="images/tematy_after_title.png" alt="Proponowane tematy">
            </div>
            <ul>
                <li>Polski śpiewnik narodowy</li>
                <li>O świątecznej porze w polskim folklorze</li>
                <li>Największe hity kompozytorskiej elity</li>
                <li>Europejskie wojaże pełne muzycznych wrażeń</li>
                <li>Klasyczne przeboje na różne nastroje</li>
                <li>Zagadkowe dźwięki bajkowej i filmowej piosenki</li>
            </ul>

        </div>
    </div>
    <div id="oferty">
        <div class="content">
            <div id="oferty-title">
                <img src="images/oferty_title.png" alt="Oferujemy także">
            </div>
            <div class="text-top">
                <p class="title-top">Teatrzyk na Kółeczkach</p>

                <p class="title-bottom">– spektakle dla najmłodszych.</p>

                <p>"Ryby śpiewają na niby" to propozycja spektaklu muzycznego dla dzieci. Oparty jest na znanych tekstach Jana Brzechwy  takich, jak między innymi "Chrząszcz", "Żuraw i Czapla”, "Stonoga", "Sójka". Lubiane przez dzieci i dorosłych teksty ubogacają całkiem nowe współczesne aranżacje. Całość dopełnia prosta i kolorowa scenografia, którym głównym bohaterem jest parasol. Dzieci mogą zapoznać się z tekstami, których być może jeszcze nie znają,a dorośli wracają w świat dzieciństwa. W spektaklu bierze udział trójka profesjonalnych aktorów.</p>
            </div>
        </div>
    </div>
    <div id="kontakt">
        <div class="content">
            <div id="kontakt-title">
                <img src="images/kontakt_title.png" alt="Skontaktuj się z nami">
            </div>
            <div class="email"><span>@:</span> kontakt@mobilnafilharmonia.pl</div>
            <div class="phone"><span>tel.</span> 511 - 64 - 77 - 37</div>
            <div class="phone"><span>tel.</span> 505 - 56 - 66 - 88</div>
            <form class="contact-form" method="post" onsubmit="return sendContact();">
                <div class="form-row">
                    <input id="contact-name" type="text" name="name" placeholder="Imię i nazwisko">
                </div>
                <div class="form-row">
                    <input id="contact-email" type="text" name="email" placeholder="Adres e-mail">
                </div>
                <div class="form-row">
                    <input id="contact-phone" type="text" name="phone" placeholder="Numer telefonu">
                </div>
                <div class="form-row">
                    <textarea id="contact-content" name="content" placeholder="Treść wiadomości"></textarea>
                </div>
                <div class="form-buttons">
                    <button type="submit">Wyślij</button>
                </div>
            </form>
            <div class="footer">
                <div class="copyright">Wszelkie prawa zastrzeżone przez <span>Mobilna Filharmonia</span></div>
                <div class="design">Projekt i realizacja: <a href="http://www.lemonadestudio.pl/" title="Agencja Reklamowa" target="_blank"></a></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="http://www.youtube.com/player_api"></script>
<script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="js/qtip/jquery.qtip.min.js" type="text/javascript"></script>
<script src="js/scripts.js/ver=1.2.33" type="text/javascript"></script>
</body>
</html>