$(document).on('click', '#show-kontakt', function (event) {
    event.preventDefault();
    var id = $(this).attr('href');
    var position = $(id).position().top;

    $('html, body').animate({scrollTop: position}, 2000);
});

$(document).on('mouseenter', '#fb', function () {
    $('#fb').animate({right: 0}, 750);
});

$(document).on('mouseleave', '#fb', function () {
    $('#fb').animate({right: -360}, 750);
});

function sendContact() {
    $('div[id^="qtip-"]').each(function () {
        _qtip2 = $(this).data("qtip");
        if (_qtip2 != undefined) {
            _qtip2.destroy(true);
        }
    });

    var send = true;
    var data = {
        name: $('#contact-name').val(),
        email: $('#contact-email').val(),
        phone: $('#contact-phone').val(),
        content: $('#contact-content').val()
    };

    if (data.name.length == 0) {
        send = false;
        showError('#contact-name', 'Wypełnij pole.')
    }

    if (data.email.length == 0) {
        send = false;
        showError('#contact-email', 'Wypełnij pole.')
    } else {
        if (!checkEmail(data.email)) {
            send = false;
            showError('#contact-email', 'Podaj poprawny adres e-mail.')
        }
    }

    if (data.content.length == 0) {
        send = false;
        showError('#contact-content', 'Wypełnij pole.')
    }

    if (send) {
        $.ajax({
            type: 'post',
            url: '/send.php',
            data: data,
            success: function (response) {
                if (response === 'OK') {
                    $('#contact-name').val('');
                    $('#contact-email').val('');
                    $('#contact-phone').val('');
                    $('#contact-content').val('');
                    alert('Twoja wiadomość została wysłana.');
                    fbq('track', 'Search', {
                        search_string: 'leather sandals'
                    });
                } else {
                    alert('Wystąpił błąd podczas wysyłania Twojej wiadomości.')
                }
            }
        });
    }

    return false;
}

function checkEmail(emailAddress) {
    var re = /^[a-zA-Z0-9._\-]+@[a-zA-Z0-9.\-]+\.[a-zA-Z]{2,6}$/;
    if (re.test(emailAddress)) {
        return true;
    }

    return false;
}

function showError(field, error) {
    $(field).qtip({
        content: {
            title: null,
            text: error
        },
        position: {
            my: 'bottom left',
            at: 'top right',
            adjust: {
                x: -30
            }
        },
        show: {
            ready: true,
            event: false
        },
        hide: {
            event: 'click focus unfocus'
        },
        style: {
            classes: 'qtip-red qtip-rounded qtip-shadow'
        }
    });
}
