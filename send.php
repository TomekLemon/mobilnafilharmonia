<?php
/**
 * Created by PhpStorm.
 * User: rafalglazar
 * Date: 09.09.2015
 * Time: 09:15
 */

require_once __DIR__ . '/vendor/swiftmailer/swiftmailer/lib/swift_required.php';

$name = $_POST['name'];
$email = $_POST['email'];
$phone = $_POST['phone'];
$content = $_POST['content'];

$message_txt = '<h3>Wiadomość z formularza kontaktowego:</h3>';
$message_txt .= nl2br($content) . '<br />';
$message_txt .= '<h3>Pozostałe dane:</h3>';
$message_txt .= 'Imię i nazwisko: ' . $name . '<br />';
$message_txt .= 'Adres e-mail: ' . $email;
if (!empty($phone)) {
    $message_txt .= '<br />' . 'Telefon: ' . $phone . '<br />';
}

$message = Swift_Message::newInstance();
$message->setSubject('Wiadomość z formularza kontaktowego');
$message->setFrom(array('mailer@mobilnafilharmonia.pl' => 'Mobilna Filharmonia / Mailer'));
$message->setTo('kontakt@mobilnafilharmonia.pl');
$message->setBody($message_txt, 'text/html');
$message->addPart(strip_tags($message_txt), 'text/plain');

if (!empty($email)) {
    $message->setReplyTo($email);
}

$transport = Swift_SmtpTransport::newInstance('lemonade.nazwa.pl', 587);
$transport->setUsername('mailer@mobilnafilharmonia.pl');
$transport->setPassword('L*0f?4/wzYy1');

$mailer = Swift_Mailer::newInstance($transport);

$result = $mailer->send($message);

if ($result) {
    echo 'OK';
} else {
    echo 'ERROR';
}
